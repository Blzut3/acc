/*
** Copyright (c) 2010, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __COMPILER_H__
#define __COMPILER_H__

#include "lib/hashmap.h"
#include "lib/stream.h"
#include "lib/string.h"

class Scanner;
class ExpressionNode;
class Symbol;
class TypeHierarchy;

class Compiler
{
	public:
		Compiler();
		~Compiler();

		ACCPP::String	Compile(const ACCPP::String &in);
		ACCPP::String	Compile(Scanner &sc);

		/**
		 * Returns an assembler tag for the given string.  Creates one if it's
		 * not already in the table.
		 */
		ACCPP::String		GetString(const ACCPP::String &lookup);
		const TypeHierarchy	&GetTypes() const { return *types; }
		Symbol				*FindSymbol(const ACCPP::String &name);
	protected:
		void			DumpStringTable();
		ACCPP::String	ParseAssemblyBlock(Scanner &sc);
		ACCPP::String	ParseScript(Scanner &sc);
		void			PopScope();
		void			PushScope();

		ACCPP::Stream	out;

		unsigned int currentScope;
		ACCPP::HashMap<ACCPP::String, ACCPP::String> stringTable;
		ACCPP::HashMap<ACCPP::String, Symbol> symbolTable;
		TypeHierarchy *types;
};

#endif /* __COMPILER_H__ */
