/*
** Copyright (c) 2010, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __PREPROCESSOR_H__
#define __PREPROCESSOR_H__

#include "lib/hashmap.h"
#include "lib/linkedlist.h"
#include "lib/string.h"

class DefineConstant
{
	public:
		DefineConstant(const ACCPP::String &name, const ACCPP::String &replacement, short numVars=NULL, const char* const varList[]=NULL);

		ACCPP::String		DoReplace(const char* const parameters[]) const;
		const ACCPP::String	&GetName() const { return name; }
		short				GetNumParameters() const { return numVars; }
	protected:
		struct Replace
		{
			public:
				int		position;
				short	index;
				int		length;
		};

		const ACCPP::String			name;
		ACCPP::String				replacement;
		ACCPP::LinkedList<Replace>	replaces;
		const short					numVars;
};

class Preprocessor
{
	public:
		void			AddDefine(const DefineConstant &constant);
		ACCPP::String	ProcessCode(const char* filename, bool importing=false, bool dumpImports=true);

		const ACCPP::String	GetLibraryName() const { return libName; }
		unsigned int		GetNumImports() const { return importList.Size(); }
		void				GetImports(ACCPP::String* buffer) const;
	protected:
		ACCPP::HashMap<ACCPP::String, const DefineConstant>	constantMap;

		ACCPP::String						libName;
		ACCPP::LinkedList<ACCPP::String>	importList;
};

#endif /* __PREPROCESSOR_H__ */
