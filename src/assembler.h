/*
** Copyright (c) 2010, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __ASSEMBLER_H__
#define __ASSEMBLER_H__

#include "lib/hashmap.h"
#include "lib/linkedlist.h"
#include "lib/string.h"

class Scanner;
namespace ACCPP { class Stream; }

class Assembler
{
	public:
		Assembler();
		~Assembler();

		ACCPP::String	Assemble(const ACCPP::String &in);
		ACCPP::String	Assemble(Scanner &sc);

	protected:
		class ByteCode;
		class Constant
		{
			private:
				enum Type
				{
					DYNAMIC,
					STATIC,
					CONSTANT
				};

				union
				{
					const ByteCode	*pCode;
					int				integer;
				} data;
				ACCPP::String	label;
				Type		type;
			public:
				Constant(const ACCPP::String &str);
				Constant(const ByteCode *pCode);
				Constant(int integer=0);

				int	Resolve(const Assembler *assembler=NULL) const;
		};
		friend class Constant;
		struct Function;
		struct MapArray;
		struct MapVariable;
		struct Script;
		struct StringTableObject;
		typedef ACCPP::LinkedList<StringTableObject> StringTable;

		unsigned int	GetByteCodeSize() const;
		void			Optimize();
		void			Parse(Scanner &sc);
		Constant		ParseConstant(Scanner &sc) const;
		void			ParseFunctions(Scanner &sc);
		void			ParseLoad(Scanner &sc);
		void			ParseMapArrays(Scanner &sc);
		void			ParseMapVariables(Scanner &sc);
		void			ParseMnemonic(Scanner &sc, bool direct);
		void			ParseScriptsDirectory(Scanner &sc);
		void			ParseStringTable(Scanner &sc);
		const Constant	*ResolveLabel(const ACCPP::String &label) const;

		// Use GetStringTableSize to assign offsets to the string table.
		static unsigned int	GetStringTableSize(StringTable &table, unsigned int offset);
		static void			WriteStringTable(ACCPP::Stream &out, StringTable &table, bool wasteful=false, bool encrypt=false);

		enum
		{
			CODE_TAIL,
			CODE_HEAD
		};
		ByteCode		*code[2];

		ACCPP::LinkedList<ACCPP::String>	loadImports;
		ACCPP::LinkedList<Function>			functions;
		ACCPP::LinkedList<MapArray>			mapArrays;
		ACCPP::LinkedList<MapVariable>		mapVariables;
		ACCPP::HashMap<unsigned short, ACCPP::String> mapVariableNames; // Arrays and Variables.
		ACCPP::LinkedList<Script>			scripts;
		StringTable							stringTable;
		unsigned short						namedScriptIndex;

	private:
		struct Instruction;
		typedef ACCPP::HashMap<ACCPP::String, Instruction *> InstructionMap;
		typedef ACCPP::HashMap<ACCPP::String, Constant> LabelIndex;
		InstructionMap	instructionMap;
		LabelIndex		labelIndex;

		Instruction			*InstructionByMnemonic(const ACCPP::String &name);
};

#endif /* __ASSEMBLER_H__ */
