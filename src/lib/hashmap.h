/*
** Copyright (c) 2012, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __ACCPP_HASHMAP_H__
#define __ACCPP_HASHMAP_H__

#include "lib/linkedlist.h"

namespace ACCPP {

/* Basic HashMap class with separate chaining. Probably could be replaced
 * with something more efficient.
 */
template<class K, class T> class HashMap
{
private:
	class Pair
	{
	public:
		Pair(K key, T obj) : key(key), obj(obj) {}

		K key;
		T obj;
	};

	typedef LinkedList<Pair> Chain;

	Pair *FindPair(const K key)
	{
		const unsigned int hash = MakeHash(&key, sizeof(K));
		Chain &chain = chains[hash%NUM_CHAINS];

		typename Chain::Iterator iter = chain.Head();
		while(iter.Next())
		{
			if(iter->key == key)
			{
				return &*iter;
				break;
			}
		}
		return NULL;
	}
	const Pair *FindPair(const K key) const
	{
		const unsigned int hash = MakeHash(&key, sizeof(K));
		const Chain &chain = chains[hash%NUM_CHAINS];

		typename Chain::ConstIterator iter = chain.HeadConst();
		while(iter.Next())
		{
			if(iter->key == key)
			{
				return &*iter;
				break;
			}
		}
		return NULL;
	}

public:
	class Iterator
	{
	public:
		Iterator(HashMap &map) :
			chainNumber(0), currentChain(&map.chains[0])
		{
			iter = currentChain->Head();
		}

		K Key() const { return iter->key; }

		bool Next()
		{
			if(iter.Next())
				return true;

			do
			{
				if(chainNumber == NUM_CHAINS)
					return false;
				++chainNumber;
				++currentChain;
			}
			while(!iter.Next());
			return true;
		}

		T &operator*() const { return iter->obj; }
		T *operator->() const { return &iter->obj; }
	private:
		friend class HashMap;

		unsigned int chainNumber;
		Chain *currentChain;
		typename Chain::Iterator iter;
	};
	class ConstIterator
	{
	public:
		ConstIterator(const HashMap &map) :
			chainNumber(0), currentChain(&map.chains[0])
		{
			iter = currentChain->HeadConst();
		}

		K Key() const { return iter->key; }

		bool Next()
		{
			if(iter.Next())
				return true;

			do
			{
				++currentChain;
				if(++chainNumber == NUM_CHAINS)
					return false;
				iter = currentChain->HeadConst();
			}
			while(!iter.Next());
			return true;
		}

		const T &operator*() const { return iter->obj; }
		const T *operator->() const { return &iter->obj; }
	private:
		friend class HashMap;

		unsigned int chainNumber;
		const Chain *currentChain;
		typename Chain::ConstIterator iter;
	};

	HashMap() : size(0) {}

	void Delete(Iterator &position)
	{
		position.currentChain->Delete(position.iter);
	}

	T *Find(const K key)
	{
		Pair *pair = FindPair(key);
		if(pair)
			return &pair->obj;
		return NULL;
	}

	const T *Find(const K key) const
	{
		const Pair *pair = FindPair(key);
		if(pair)
			return &pair->obj;
		return NULL;
	}

	T &Insert(const K key, const T &obj)
	{
		const unsigned int hash = MakeHash(&key, sizeof(K));
		Chain &chain = chains[hash%NUM_CHAINS];
		++size;

		return chain.Push(Pair(key, obj)).obj;
	}

	unsigned int Size() const { return size; }

	T &operator[] (const K key)
	{
		Pair *pair = FindPair(key);
		if(pair)
			return pair->obj;
		return Insert(key, T());
	}

	const T &operator[] (const K key) const
	{
		const Pair *pair = FindPair(key);
		if(pair)
			return pair->obj;
		return T();
	}

private:
	static const unsigned int NUM_CHAINS = 29;
	Chain chains[NUM_CHAINS];
	unsigned int size;
};

template<class K, class T> class MultiHashMap : public HashMap<K, LinkedList<T> >
{
private:
	typedef HashMap<K, LinkedList<T> > Super;

public:
	typedef typename LinkedList<T>::Iterator KeyIterator;
	typedef typename LinkedList<T>::ConstIterator KeyConstIterator;

	KeyIterator Find(const K key)
	{
		LinkedList<T> *ret = Super::Find(key);
		if(ret)
			return ret->Head();
		return KeyIterator();
	}

	const KeyConstIterator Find(const K key) const
	{
		const LinkedList<T> *ret = Super::Find(key);
		if(ret)
			return ret->HeadConst();
		return KeyConstIterator();
	}
};

/* Simple generic hash function that will take any object and produce a hash
 * for it.
 */
unsigned int MakeHash(const void *key, size_t size);

}

#endif
