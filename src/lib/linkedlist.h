/*
** Copyright (c) 2012, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __ACCPP_LINKEDLIST_H__
#define __ACCPP_LINKEDLIST_H__

#include <cstdlib>

namespace ACCPP {

/**
 * Single linked list. Includes efficient iteration as well as insertion at
 * the end of the list.
 */
template<class T> class LinkedList
{
private:
	class Node
	{
	public:
		Node(const T &obj) : obj(obj), next(NULL) {}

		Node *next;
		T obj;
	};

public:
	// On top of iterating this keeps track of the previous node so we can
	// delete/insert with them efficiently.
	class Iterator
	{
	public:
		Iterator() : node(NULL), prev(NULL) {}
		Iterator(LinkedList &list) : node(list.sentinal), prev(NULL) {}

		bool Next()
		{
			prev = node;
			if(!node->next)
				return false;
			node = node->next;
			return true;
		}

		T &operator*() const { return node->obj; }
		T *operator->() const { return &node->obj; }
	private:
		friend class LinkedList;

		Node *node;
		Node *prev;
	};
	class ConstIterator
	{
	public:
		ConstIterator() : node(NULL) {}
		ConstIterator(const LinkedList &list) : node(list.sentinal) {}

		bool Next()
		{
			if(!node || !node->next)
				return false;
			node = node->next;
			return true;
		}

		const T &operator*() const { return node->obj; }
		const T *operator->() const { return &node->obj; }
	private:
		friend class LinkedList;

		const Node *node;
	};

	LinkedList() : size(0)
	{
		tail = sentinal = reinterpret_cast<Node *>(malloc(sizeof(Node)));
		sentinal->next = NULL;
	}
	LinkedList(const LinkedList &other)
	{
		tail = sentinal = reinterpret_cast<Node *>(malloc(sizeof(Node)));
		sentinal->next = NULL;

		operator=(other);
	}
	~LinkedList()
	{
		Clear();
		free(sentinal);
	}

	void Clear()
	{
		Iterator iter = Head();
		Node *deleteMe = NULL;
		while(iter.Next())
		{
			delete deleteMe;
			deleteMe = iter.node;
		}
		delete deleteMe;

		sentinal->next = NULL;
		tail = sentinal;
	}

	Iterator Head() { return Iterator(*this); }
	ConstIterator HeadConst() const { return ConstIterator(*this); }

	void Delete(Iterator &position)
	{
		Node * const prev = position.prev;
		Node * &node = position.node;

		prev->next = node->next;
		delete node;

		// Back track the iterator a bit.
		node = prev;
	}

	void InsertAfter(Iterator position, const T &obj)
	{
		Node * const newNode = new Node(obj);
		Node * &next = position.node->next;

		newNode->next = next;
		next = newNode;
		if(next == NULL)
			tail = newNode;

		++size;
	}

	T &Push(const T &obj)
	{
		Node *oldTail = tail;
		tail = new Node(obj);
		tail->next = NULL;
		oldTail->next = tail;

		++size;
		return tail->obj;
	}

	unsigned int Size() const
	{
		return size;
	}

	LinkedList &operator=(const LinkedList<T> &other)
	{
		Clear();

		ConstIterator iter = other.HeadConst();
		while(iter.Next())
			Push(*iter);

		return *this;
	}

private:
	Node *sentinal;
	Node *tail;
	unsigned int size;
};

}

#endif
