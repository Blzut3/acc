/*
** Copyright (c) 2012, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __ACCPP_STACK_H__
#define __ACCPP_STACK_H__

namespace ACCPP {

/**
 * Simple last in, first out stack.
 */
template<class T> class Stack
{
public:
	Stack() : top(NULL) {}
	Stack(const Stack &other) : top(NULL) { operator=(other); }
	~Stack() { while(top) { Pop(); } }

	void Push(const T &obj)
	{
		top = new Item(obj, top);
	}

	T Pop()
	{
		return top ? top->Pop(top) : T();
	}

	Stack &operator=(const Stack &other)
	{
		// Copy the stack while keeping the order.
		Item *otherStack = top;
		Item **copy = &top;
		for(Item *otherStack = other.top;otherStack;otherStack = otherStack->next)
		{
			*copy = new Item(otherStack->item, NULL);
			copy = &(*copy)->next;
		}
	}
private:
	class Item
	{
	public:
		Item(const T &obj, Item *top) : item(obj), next(top) {}
		T Pop(Item *&top)
		{
			top = next;
			const T ret = item;

			next = NULL;
			delete this;
			return ret;
		}

	private:
		Item *next;
		T item;
	} *top;
};

}

#endif
