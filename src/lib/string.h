/*
** Copyright (c) 2012, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __ACCPP_STRING_H__
#define __ACCPP_STRING_H__

#include <cstdlib>

namespace ACCPP {

/* Basic string library. This was created to avoid the complexities of the
 * C++ STD library. It is a basic reference counting string.
 * 
 * I've considered using ZDoom's FString but pulling in the dependencies for
 * the Format() function didn't seem like a particularly good idea. I
 * believe I also looked at EE's strings, but didn't care for it for some
 * reason I can't remember (plus I would have had to have gotten it
 * relicensed.
 */
class String
{
public:
	String();
	String(const char* str, int len=-1) : ref(NULL) { CopyString(str, len); }
	String(const String &other) : ref(NULL) { this->operator=(other); }
	~String();

	inline const char* Chars() const { return ref->Chars(); }
	int Compare(const String &other) const;
	int CompareNoCase(const String &other) const;
	String Concat(char character) const;
	String Concat(const String &other) const;
	String &Erase(int start, int len=-1);
	const char *EncryptedChars(unsigned int key);
	int IndexOf(char character, int start=0) const;
	int IndexOf(const char* str, int start=0) const;
	void Insert(int pos, const String &other);
	inline bool IsEmpty() const { return Length() == 0; }
	int LastIndexOf(char character, int start=-1) const;
	int LastIndexOf(const char* str, int start=-1) const;
	inline unsigned int Length() const { return ref->Length(); }
	String &Replace(int pos, int len, const String &replacement);
	String Substr(int start, int len=-1) const;
	String ToUpper() const;

	inline String operator+(char character) const { return Concat(character); }
	inline String operator+(const String &other) const { return Concat(other); }

	inline String &operator=(const char* str) { CopyString(str); return *this; }
	String &operator=(const String &other);
	inline String &operator+=(char character) { return this->operator=(Concat(character)); }
	inline String &operator+=(const String &other) { return this->operator=(Concat(other)); }

	inline char operator[](int index) const { return Chars()[index]; }
	inline operator const char*() const { return Chars(); }

	inline bool operator==(const String &other) const { return Compare(other) == 0; }
	inline bool operator<(const String &other) const { return Compare(other) < 0; }
	inline bool operator>(const String &other) const { return Compare(other) > 0; }

protected:
	void CopyString(const char* str, int len=-1);
	bool FixBounds(int &pos, int &len) const;

private:
	class StringRef
	{
	public:
		StringRef(const char* str, int len=-1);
		StringRef(const StringRef &other);
		~StringRef();

		void Dereference();
		void Reference();

		inline const char* Chars() const { return data; }
		inline unsigned int Length() const { return length; }

		unsigned int length;
		char* data;
		char* encrypted;
		unsigned int encryptionKey;
		unsigned int references;
	} *ref;
};

unsigned int MakeHash(const String *key, size_t size);

}

#endif
