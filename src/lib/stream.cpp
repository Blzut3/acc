/*
** Copyright (c) 2012, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "lib/stream.h"

#include <cstring>
#include <cstdio>

using namespace ACCPP;

#define STREAM_BLOCK_SIZE 2048

Stream::Stream() : maxSize(STREAM_BLOCK_SIZE), size(0), position(0)
{
	bytes = new unsigned char[maxSize];
}

Stream::~Stream()
{
	delete[] bytes;
}

void Stream::Clear()
{
	size = 0;
	position = 0;
}

void Stream::CheckBuffer(unsigned int length)
{
	if(position + length > maxSize)
		Expand(size + length);
}

void Stream::Expand(unsigned int requestSize)
{
	const unsigned char* const old = bytes;
	const unsigned int oldSize = maxSize;

	maxSize = (requestSize/STREAM_BLOCK_SIZE + 1)*STREAM_BLOCK_SIZE;
	bytes = new unsigned char[maxSize + STREAM_BLOCK_SIZE];
	memcpy(bytes, old, oldSize);
	delete[] old;
}

void Stream::IncrementPos(unsigned int length)
{
	position += length;
	if(position > size)
		size = position;
}

void Stream::Seek(unsigned int pos)
{
	position = pos;
}

void Stream::Write(const char* data, int length)
{
	if(length < 0)
		length = strlen(data);

	CheckBuffer(length);

	memcpy(bytes+position, data, length);

	IncrementPos(length);
}

void Stream::Write(int number)
{
	char buffer[12];
	sprintf(buffer, "%d", number);
	Write(buffer, strlen(buffer));
}
