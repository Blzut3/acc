/*
** Copyright (c) 2012, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "main.h"
#include "lib/hashmap.h"
#include "lib/string.h"

#include <cstdlib>

namespace ACCPP {

unsigned int MakeHash(const void *key, size_t size)
{
	size_t pos = 0;
	unsigned int hash = 0;
	const unsigned char *hashData = reinterpret_cast<const unsigned char*>(key);
	while(pos < size)
	{
		if(size - pos < 4)
		{
			if(size - pos == 1)
				hash ^= *hashData++;
			else if(size - pos == 2)
			{
				hash ^= *(unsigned short*)hashData;
				hashData += 2;
			}
			else
			{
				hash ^= (*(unsigned short*)hashData)|*(hashData+2);
				hashData += 3;
			}
		}
		else
		{
			hash ^= *(unsigned int*)hashData;
			hashData += 4;
		}
		pos += 4;
	}
	return hash;
}

unsigned int MakeHash(const String *key, size_t size)
{
	return MakeHash(key->Chars(), key->Length());
}

}
