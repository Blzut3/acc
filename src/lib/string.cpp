/*
** Copyright (c) 2012, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "lib/string.h"

#include <cstring>
#include <cctype>

using namespace ACCPP;

String::StringRef::StringRef(const char* str, int len) : encrypted(NULL)
{
	if(len == -1)
		len = strlen(str);

	length = len;
	data = new char[length+1];
	strncpy(data, str, length);
	data[length] = 0;

	references = 1;
}
String::StringRef::StringRef(const StringRef &other)
{
	length = other.length;
	data = new char[length+1];
	strncpy(data, other.data, length);
	data[length] = 0;
}
String::StringRef::~StringRef()
{
	delete[] data;
	delete[] encrypted;
}

void String::StringRef::Dereference()
{
	if(--references == 0)
		delete this;
}
void String::StringRef::Reference() { ++references; }

//---------------------------------------------------------------------------

String::String()
{
	static StringRef nullString("", 0);

	ref = &nullString;
	ref->Reference();
}

String::~String()
{
	if(ref)
		ref->Dereference();
}

int String::Compare(const String &other) const
{
	if(ref == other.ref)
		return 0;
	return strcmp(ref->Chars(), other);
}

int String::CompareNoCase(const String &other) const
{
	if(ref == other.ref)
		return 0;
	return stricmp(ref->Chars(), other);
}

String String::Concat(char character) const
{
	char* newString = new char[Length()+2];
	strncpy(newString, ref->Chars(), Length());
	newString[Length()] = character;
	newString[Length()+1] = 0;

	String tmp(newString, Length()+1);
	delete[] newString;
	return tmp;
}

String String::Concat(const String &other) const
{
	const unsigned int newLength = Length() + other.Length();
	char* newString = new char[newLength+1];
	strncpy(newString, ref->Chars(), Length());
	strncpy(newString+Length(), other, other.Length());
	newString[newLength] = 0;

	String tmp(newString, newLength);
	delete[] newString;
	return tmp;
}

void String::CopyString(const char* str, int len)
{
	if(ref)
		ref->Dereference();
	ref = new StringRef(str, len);
}

String &String::Erase(int pos, int len)
{
	if(!FixBounds(pos, len))
		return *this;

	operator=(Substr(0, pos) + Substr(pos+len));
	return *this;
}

const char* String::EncryptedChars(unsigned int key)
{
	key *= 157135;
	if(ref->encrypted == NULL || ref->encryptionKey != key)
	{
		delete ref->encrypted;
		ref->encrypted = new char[Length()+2];
		memcpy(ref->encrypted, ref->data, Length()+1);
		ref->encrypted[Length()+1] = 0;

		unsigned int i = 0;
		while(i <= Length())
			ref->encrypted[i] ^= key+((i++)>>1);
	}
	return ref->encrypted;
}

bool String::FixBounds(int &pos, int &len) const
{
	if(len == -1)
		len = Length();

	// Fix bounds
	if(pos > Length())
		return false;
	else if(pos < 0)
	{
		len += pos;
		pos = 0;
	}
	if(pos + len > Length())
		len = Length()-pos;
	if(len < 1)
		return false;
	return true;
}

int String::IndexOf(char character, int start) const
{
	const char lstr[2] = { character, 0 };
	return IndexOf(lstr, start);
}

int String::IndexOf(const char* lstr, int start) const
{
	if(start >= Length())
		return -1;

	const char* str = Chars()+start;
	const char* const end = Chars()+Length();
	while(*str != *lstr || strncmp(str, lstr, strlen(lstr)) != 0)
	{
		if(++str == end)
			return -1;
	}

	return str - Chars();
}

void String::Insert(int pos, const String &other)
{
	if(pos < 0)
		pos = 0;
	else if(pos > Length())
		pos = Length();
	else if(pos == Length())
	{
		operator+=(other);
		return;
	}

	operator=(Substr(0, pos) + other + Substr(pos));
}

int String::LastIndexOf(char character, int start) const
{
	const char lstr[2] = { character, 0 };
	return LastIndexOf(lstr, start);
}

int String::LastIndexOf(const char* lstr, int start) const
{
	if(start == -1 || start >= Length())
		start = Length()-1;

	const char* str = Chars()+start;
	while(*str != *lstr || strncmp(str, lstr, strlen(lstr)) != 0)
	{
		// See if we've hit the beginning of the string.
		if(str-- == Chars())
			return -1;
	}

	return str - Chars();
}

String &String::Replace(int pos, int len, const String &replacement)
{
	if(!FixBounds(pos, len))
		return *this;

	operator=(Substr(0, pos) + replacement + Substr(pos+len));
	return *this;
}

String String::Substr(int start, int len) const
{
	const unsigned int strlen = Length();
	if(start > strlen)
		return String();
	if(start < 0)
		start += strlen;
	if(len > strlen - start)
		len = strlen - start;

	return String(ref->Chars()+start, len);
}

String String::ToUpper() const
{
	String copy;
	copy.CopyString(Chars(), Length());
	char* data = copy.ref->data;
	for(unsigned int i = 0;i < copy.Length();++i)
		data[i] = toupper(data[i]);
	return copy;
}

String &String::operator=(const String &other)
{
	if(ref)
		ref->Dereference();
	ref = other.ref;
	ref->Reference();
	return *this;
}
