/*
** Copyright (c) 2010, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "assembler.h"
#include "main.h"
#include "scanner.h"
#include "lib/hashmap.h"
#include "lib/linkedlist.h"
#include "lib/stream.h"
#include "lib/string.h"

#include <cstring>

using namespace ACCPP;

// Instructions ----------------------------------------------------------------

struct Assembler::Instruction
{
	public:
		unsigned int	opcode;
		String			name;
		unsigned char	args;
		bool			argsVariable;
		String			argFormat;

		bool			direct;
		Instruction		*alternate;
};

// Byte Code -------------------------------------------------------------------

// Linked List structure for assembled code.
class Assembler::ByteCode
{
	public:
		ByteCode() : args(NULL), opcode(NULL), offset(0), labeled(false), next(NULL)
		{
		}
		~ByteCode()
		{
			if(args != NULL)
				delete[] args;

			delete next;
		}

		void			Allocate(Assembler::Instruction *opcode)
		{
			this->opcode = opcode;
			if(args != NULL)
				delete[] args;
			args = new Assembler::Constant[opcode->args];
			numArgs = opcode->args;
		}

		void			AllocateArgumentSpace(unsigned int numIndex)
		{
			unsigned int oldSize = GetSize();
			numArgs += args[numIndex].Resolve();
			Assembler::Constant* newArgs = new Assembler::Constant[numArgs];
			memcpy(newArgs, args, opcode->args*sizeof(Assembler::Constant));
			delete[] args;
			args = newArgs;
			if(next != NULL)
				PushOffset(GetSize() - oldSize);
		}

		bool			IsLabeled() const
		{
			return labeled;
		}

		bool			IsNull() const
		{
			return opcode == NULL;
		}

		bool			HasNext() const
		{
			return next != NULL && !next->IsNull();
		}

		const Assembler::Constant	&GetArgument(unsigned int i) const
		{
			if(i >= numArgs)
				return unresolved;
			return args[i];
		}

		unsigned int	GetNumArguments() const
		{
			return numArgs;
		}

		unsigned int	GetOffset() const
		{
			return offset;
		}

		unsigned int	GetSize() const
		{
			unsigned int i = 0;
			unsigned int size = 4;
			if(g_format == FORMAT_ZDOOM)
			{
				if(opcode == NULL)
					return 1;
				else
					size = (opcode->opcode >= 240 ? 2 : 1);
			}
			if(opcode == NULL)
				return size;
			const Assembler::Constant *lastValue = &unresolved;
			char lastType = '\0';
			for(const char* iter = opcode->argFormat;*iter != '\0';++iter)
			{
				switch(*iter)
				{
					default:
						size += 4;
						break;
					case 'S':
						size += 2;
						break;
					case 'B':
						size += 1;
						break;
					case '-':
						size += lastValue->Resolve()*(lastType == 'B' ? 1 : (lastType == 'S' ? 2 : 4));
						break;
					case '+':
						size += ((offset+size)%4 != 0 ? 4 - ((offset+size)%4) : 0);
						break;
				}
				if(*iter == '+')
					continue;
				else if(*iter != '-')
				{
					lastValue = &GetArgument(i++);
					lastType = *iter;
				}
				else
					i += lastValue->Resolve();
			}
			return size;
		}

		ByteCode		*Next()
		{
			// Nothing here, so don't waste space.
			if(opcode == NULL)
				return this;

			if(next == NULL)
			{
				next = new ByteCode();
				next->offset = offset + GetSize();
			}
			return next;
		}

		const Assembler::Instruction	*OpCode() const
		{
			return opcode;
		}

		void			PushOffset(int offset)
		{
			unsigned int size = GetSize();
			this->offset += offset;
			if(next != NULL)
			{
				// Check to make sure alignment is correct.
				if(opcode->argFormat[0] == '+')
					next->PushOffset(offset + (GetSize() - size));
				else
					next->PushOffset(offset);
			}
		}

		void			Relink(ByteCode *next)
		{
			this->next = next;
			// Recalculate the offsets
			next->offset = offset + GetSize();
			if(next->next != NULL)
				next->Relink(next->next);
		}

		void			SetArgument(unsigned int index, const Assembler::Constant &value)
		{
			if(index >= numArgs)
				return;
			args[index] = value;
		}

		void			SetLabeled(bool labeled)
		{
			this->labeled = labeled;
		}

		unsigned int	SetToAlternate(ByteCode *orig)
		{
			unsigned int i;

			Allocate(orig->OpCode()->alternate);
			for(i = 0;i < orig->numArgs && i < numArgs;i++)
				args[i] = orig->args[i];

			return i;
		}

		void			Unlink()
		{
			next = NULL;
		}

	protected:
		const Assembler::Instruction *opcode;
		unsigned int			numArgs;
		Assembler::Constant*	args;
		unsigned int			offset;

		bool					labeled;
		Assembler::ByteCode		*next;

	private:
		static const Assembler::Constant	unresolved;
};
const Assembler::Constant Assembler::ByteCode::unresolved;

// Data Types ------------------------------------------------------------------

struct Assembler::Script
{
	public:
		int		number;
		int		type;
		int		flags;
		short	args;
		String	address;
		short	varCount;
		String	name;
};

struct Assembler::StringTableObject
{
	public:
		String			str;
		unsigned int	offset;
};

struct Assembler::Function
{
	public:
		short	numArgs;
		short	numVars;
		bool	hasReturn;
		String	address;
		String	label;
};

struct Assembler::MapArray
{
	public:
		bool						import;
		bool						strings;
		unsigned int				number;
		unsigned int				size;
		LinkedList<Assembler::Constant>	values;
		String						label; // Used for imports.
};

struct Assembler::MapVariable
{
	public:
		bool				import;
		bool				isString;
		unsigned int		number;
		Assembler::Constant	value;
		String				label; // Used for imports.
};

Assembler::Constant::Constant(const String &str) : type(DYNAMIC)
{
	label = str;
}
Assembler::Constant::Constant(const ByteCode *pCode) : type(STATIC)
{
	data.pCode = pCode;
}
Assembler::Constant::Constant(int integer) : type(CONSTANT)
{
	data.integer = integer;
}

int Assembler::Constant::Resolve(const Assembler *assembler) const
{
	switch(type)
	{
		case DYNAMIC:
		{
			if(assembler == NULL)
				return 0;
			const Constant *constant = assembler->ResolveLabel(label);
			if(constant == NULL)
				return 0;
			// Should we pass the aseembler pointer into here?
			// I'm guessing no as an easy way to prevent infinite loops.
			return constant->Resolve();
		}
		case STATIC:
			if(data.pCode == NULL)
				return 0;
			return data.pCode->GetOffset();
		default:
			break;
	}
	return data.integer;
}

// Assembler -------------------------------------------------------------------

Assembler::Assembler() : namedScriptIndex(0)
{
	code[CODE_TAIL] = code[CODE_HEAD] = new ByteCode();
}

Assembler::~Assembler()
{
	delete code[CODE_HEAD];

	for(InstructionMap::Iterator iter(instructionMap);iter.Next();)
	{
		delete *iter;
	}
}

String Assembler::Assemble(const String &input)
{
	Scanner sc(input, input.Length());
	return Assemble(sc);
}

String Assembler::Assemble(Scanner &sc)
{
	Parse(sc);
	Optimize();

	Stream out;
	// Write header.
	code[CODE_HEAD]->PushOffset(8);
	unsigned int size = GetByteCodeSize()+8;

	// first 8 bytes go in the header, the rest are technically a footer.
	char header[24] = {'A','C','S',0, WriteLittleLongDirect(size), WriteLittleLongDirect(size), 'A','C','S','e', 0,0,0,0, 0,0,0,0 };
	switch(g_format)
	{
		default:
			break;
		case FORMAT_HEXEN:
			WriteLittleLong(header+4, size);
			break;
		case FORMAT_ZDOOMOLD:
			header[11] = 'E';
			break;
	}
	out.Write(header, 8);

	ByteCode *pCode = code[CODE_HEAD];
	while(pCode->OpCode() != NULL)
	{
		char data[4] = {WriteLittleLongDirect(pCode->OpCode()->opcode)};
		if(g_format == FORMAT_ZDOOM)
		{
			if(pCode->OpCode()->opcode < 240)
				out.Write(data, 1);
			else
			{
				unsigned int opcode = pCode->OpCode()->opcode;
				opcode = (((opcode - 240)>>8) + 240) + (((opcode - 240)&0xFF)<<8);
				WriteLittleShort(data, opcode);
				out.Write(data, 2);
			}
		}
		else
			out.Write(data, 4);

		if(pCode->GetNumArguments() > 0)
		{
			// Write out the arguments.  We need to scan for the width of the
			// argument as well.
			const char* type = pCode->OpCode()->argFormat;
			int lastValue = 0;
			char lastType = '\0';
			if(*type == '+')
			{
				++type;
				if(out.Size()%4 != 0)
				{
					WriteLittleLong(data, 0);
					out.Write(data, 4 - (out.Size()%4));
				}
			}
			for(unsigned int i = 0;i < pCode->GetNumArguments();i++)
			{
				WriteLittleLong(data, pCode->GetArgument(i).Resolve(this));
				if(*type != '-')
					lastValue = pCode->GetArgument(i).Resolve(this);

				if(*type == '-')
				{
					if(lastValue-- <= 0)
					{
						++type;
						lastType = *type;
					}
				}
				else
				{
					lastType = *type;
					++type;
				}

				switch(lastType)
				{
					default:
						out.Write(data, 4);
						break;
					case 'S':
						out.Write(data, 2);
						break;
					case 'B':
						out.Write(data, 1);
						break;
				}
			}
		}

		pCode = pCode->Next();
	}

	if(g_format == FORMAT_HEXEN)
	{
		// Write directory.
		char directoryHeader[4];
		WriteLittleLong(directoryHeader, scripts.Size());
		out.Write(directoryHeader, 4);
		for(LinkedList<Script>::ConstIterator iter(scripts);iter.Next();)
		{
			int number = iter->number + (iter->type*1000);
			int args = iter->args;
			const Constant *address = ResolveLabel(iter->address);
			if(address == NULL)
				sc.ScriptMessage(Scanner::ERROR, "Address \"%s\" for script %d unresolved.", iter->address.Chars(), iter->number);
			char scriptPtr[12] = {WriteLittleLongDirect(number), 0,0,0,0, WriteLittleLongDirect(args)};
			WriteLittleLong(scriptPtr+4, address->Resolve(this));
			out.Write(scriptPtr, 12);
		}

		// Write strings table.
		GetStringTableSize(stringTable, static_cast<unsigned int> (out.Size()) + 4*(stringTable.Size()+1));
		WriteStringTable(out, stringTable);
	}
	else
	{
		// Write the Scripts Directory
		char chunkHeader[8] = {'S','P','T','R',0,0,0,0};
		if(scripts.Size() > 0)
		{
			StringTable scriptNames;
			unsigned int numScriptsWithVarCount = 0;
			unsigned int numScriptsWithFlags = 0;
			WriteLittleLong(chunkHeader+4, scripts.Size()*8);
			out.Write(chunkHeader, 8);
			for(LinkedList<Script>::ConstIterator iter(scripts);iter.Next();)
			{
				const Constant *address = ResolveLabel(iter->address);
				if(address == NULL)
					sc.ScriptMessage(Scanner::ERROR, "Address \"%s\" for script %d unresolved.", iter->address.Chars(), iter->number);
				char sptr[8] = {0,0, (unsigned char)iter->type, (unsigned char)iter->args, 0,0,0,0};
				WriteLittleShort(sptr, iter->number);
				WriteLittleLong(sptr+4, address->Resolve(this));
				out.Write(sptr, 8);

				if(iter->flags != 0)
					numScriptsWithFlags++;
				if(iter->varCount != 20)
					numScriptsWithVarCount++;
				if(iter->number < 0)
				{
					StringTableObject strObj;
					strObj.str = iter->name;
					scriptNames.Push(strObj);
				}
			}

			if(numScriptsWithFlags > 0)
			{
				memcpy(chunkHeader, "SFLG", 4);
				WriteLittleLong(chunkHeader+4, numScriptsWithFlags*4);
				out.Write(chunkHeader, 8);
				for(LinkedList<Script>::ConstIterator iter(scripts);iter.Next();)
				{
					if(iter->flags == 0)
						continue;

					char data[4] = {WriteLittleShortDirect(iter->number), WriteLittleShortDirect(iter->flags)};
					out.Write(data, 4);
				}
			}

			if(numScriptsWithVarCount > 0)
			{
				memcpy(chunkHeader, "SVCT", 4);
				WriteLittleLong(chunkHeader+4, numScriptsWithVarCount*4);
				out.Write(chunkHeader, 8);
				for(LinkedList<Script>::ConstIterator iter(scripts);iter.Next();)
				{
					if(iter->varCount == 20)
						continue;

					char data[4] = {WriteLittleShortDirect(iter->number), WriteLittleShortDirect(iter->varCount)};
					out.Write(data, 4);
				}
			}

			if(namedScriptIndex > 0)
			{
				memcpy(chunkHeader, "SNAM", 4);
				unsigned int strPointersSize = 4*scriptNames.Size();
				WriteLittleLong(chunkHeader+4, 4+strPointersSize+GetStringTableSize(scriptNames, 4+strPointersSize));
				out.Write(chunkHeader, 8);
				WriteStringTable(out, scriptNames);
			}
		}

		// String table directory
		if(stringTable.Size() > 0)
		{
			unsigned int strPointersSize = 4*stringTable.Size();
			memcpy(chunkHeader, "STRL", 4);
			if(g_encryptStrings)
				chunkHeader[3] = 'E';
			WriteLittleLong(chunkHeader+4, 12+strPointersSize+GetStringTableSize(stringTable, 12+strPointersSize));
			out.Write(chunkHeader, 8);
			WriteStringTable(out, stringTable, true, g_encryptStrings);
		}

		// Library Imports
		if(loadImports.Size() > 0)
		{
			unsigned int size = 0;
			memcpy(chunkHeader, "LOAD", 4);
			for(LinkedList<String>::ConstIterator iter(loadImports);iter.Next();)
				size += iter->Length()+1;
			WriteLittleLong(chunkHeader+4, size);
			out.Write(chunkHeader, 8);
			for(LinkedList<String>::ConstIterator iter(loadImports);iter.Next();)
				out.Write(iter->Chars(), iter->Length()+1);
		}

		// Functions
		if(functions.Size() > 0)
		{
			StringTable functionNames;
			memcpy(chunkHeader, "FUNC", 4);
			WriteLittleLong(chunkHeader+4, functions.Size()*8);
			out.Write(chunkHeader, 8);
			for(LinkedList<Function>::ConstIterator iter(functions);iter.Next();)
			{
				const Constant *address = ResolveLabel(iter->address);
				if(address == NULL)
					sc.ScriptMessage(Scanner::ERROR, "Function \"%s\" undefined.", iter->address.Chars());
				char data[8] = {(unsigned char)iter->numArgs,(unsigned char)iter->numVars,iter->hasReturn,0, 0,0,0,0};
				WriteLittleLong(data+4, address->Resolve());
				out.Write(data, 8);
				StringTableObject strObj;
				strObj.str = iter->label;
				functionNames.Push(strObj);
			}

			memcpy(chunkHeader, "FNAM", 4);
			unsigned int strPointersSize = 4*functionNames.Size();
			WriteLittleLong(chunkHeader+4, 4+strPointersSize+GetStringTableSize(functionNames, 4+strPointersSize));
			out.Write(chunkHeader, 8);
			WriteStringTable(out, functionNames);
		}

		// Map Arrays
		if(mapArrays.Size() > 0)
		{
			// We'll need to size the number of imported arrays and normal.
			unsigned int arrayChunkSizes[2] = {0, 4};
			unsigned int numArrayImports = 0;
			unsigned int numStringArrays = 0;

			// To do this we might as well use the loop we use to dump init
			// data.
			for(LinkedList<MapArray>::ConstIterator iter(mapArrays);iter.Next();)
			{
				if(iter->import)
				{
					arrayChunkSizes[1] += 9 + iter->label.Length();
					numArrayImports++;
					continue;
				}
				else
				{
					arrayChunkSizes[0] += 8;
					if(iter->values.Size() == 0)
						continue;
				}

				if(iter->strings)
					numStringArrays++;

				memcpy(chunkHeader, "AINI", 4);
				WriteLittleLong(chunkHeader+4, (iter->size+1)*4);
				out.Write(chunkHeader, 8);
				char* data = new char[(iter->size+1)*4];
				char* tmp = data+4;
				WriteLittleLong(data, iter->number);
				for(LinkedList<Constant>::ConstIterator citer(iter->values);citer.Next();)
				{
					WriteLittleLong(tmp, citer->Resolve(this));
					tmp += 4;
				}
				out.Write(data, (iter->size+1)*4);
				delete[] data;
			}

			// Write the information on the arrays.
			if(arrayChunkSizes[0] > 0)
			{
				memcpy(chunkHeader, "ARAY", 4);
				WriteLittleLong(chunkHeader+4, arrayChunkSizes[0]);
				out.Write(chunkHeader, 8);
				for(LinkedList<MapArray>::ConstIterator iter(mapArrays);iter.Next();)
				{
					if(iter->import)
						continue;
					char data[8] = {WriteLittleLongDirect(iter->number), WriteLittleLongDirect(iter->size)};
					out.Write(data, 8);
				}
			}

			if(numArrayImports > 0)
			{
				memcpy(chunkHeader, "AIMP", 4);
				WriteLittleLong(chunkHeader+4, arrayChunkSizes[1]);
				out.Write(chunkHeader, 8);
				char numArrayImportsOut[4] = {WriteLittleLongDirect(numArrayImports)};
				out.Write(numArrayImportsOut, 4);
				for(LinkedList<MapArray>::ConstIterator iter(mapArrays);iter.Next();)
				{
					if(!iter->import)
						continue;
					char data[8] = {WriteLittleLongDirect(iter->number), WriteLittleLongDirect(iter->size)};
					out.Write(data, 8);
					out.Write(iter->label.Chars(), iter->label.Length()+1);
				}
			}

			if(numStringArrays > 0)
			{
				memcpy(chunkHeader, "ASTR", 4);
				WriteLittleLong(chunkHeader+4, numStringArrays*4);
				out.Write(chunkHeader, 8);
				for(LinkedList<MapArray>::ConstIterator iter(mapArrays);iter.Next();)
				{
					if(iter->import || !iter->strings)
						continue;
					char data[4] = {WriteLittleLongDirect(iter->number)};
					out.Write(data, 4);
				}
			}
		}

		// Map Variables
		if(mapArrays.Size() > 0)
		{
			// Pretty much the same deal as with arrays.
			unsigned int importChunkSize = 0;
			unsigned int numVarImports = 0;
			unsigned int numStringVars = 0;

			unsigned int varStart = 0xFFFFFFFF;
			unsigned int varEnd = 0;

			for(LinkedList<MapVariable>::ConstIterator iter(mapVariables);iter.Next();)
			{
				if(iter->import)
				{
					importChunkSize += 5 + iter->label.Length();
					numVarImports++;
					continue;
				}

				if(iter->isString)
					numStringVars++;

				if(iter->number < varStart)
					varStart = iter->number;
				if(iter->number > varEnd)
					varEnd = iter->number;
			}

			if(varEnd >= varStart)
			{
				memcpy(chunkHeader, "MINI", 4);
				WriteLittleLong(chunkHeader+4, (varEnd-varStart+2)*4);
				out.Write(chunkHeader, 8);
				char* data = new char[varStart];
				WriteLittleLong(data, varEnd-varStart+1);
				for(LinkedList<MapVariable>::ConstIterator iter(mapVariables);iter.Next();)
				{
					WriteLittleLong(data+4+(iter->number - varStart), iter->value.Resolve(this));
				}
				out.Write(data, (varEnd-varStart+2)*4);
				delete[] data;
			}

			if(numVarImports > 0)
			{
				memcpy(chunkHeader, "MIMP", 4);
				WriteLittleLong(chunkHeader+4, importChunkSize);
				out.Write(chunkHeader, 8);
				for(LinkedList<MapVariable>::ConstIterator iter(mapVariables);iter.Next();)
				{
					if(!iter->import)
						continue;
					char data[4] = {WriteLittleLongDirect(iter->number)};
					out.Write(data, 4);
					out.Write(iter->label.Chars(), iter->label.Length()+1);
				}
			}

			if(numStringVars > 0)
			{
				memcpy(chunkHeader, "MSTR", 4);
				WriteLittleLong(chunkHeader+4, numStringVars*4);
				out.Write(chunkHeader, 8);
				for(LinkedList<MapVariable>::ConstIterator iter(mapVariables);iter.Next();)
				{
					if(iter->import || !iter->isString)
						continue;
					char data[4] = {WriteLittleLongDirect(iter->number)};
					out.Write(data, 4);
				}
			}
		}

		// Generate Map array/variable String table.
		if(mapVariableNames.Size() > 0)
		{
			StringTable mapNameTable;
			unsigned short maxMapVar = 0;
			for(HashMap<unsigned short, String>::ConstIterator iter(mapVariableNames);iter.Next();)
			{
				if(iter.Key() > maxMapVar)
					maxMapVar = iter.Key();
			}
			for(unsigned short i = 0;i <= maxMapVar;i++)
			{
				StringTableObject strObj;
				String *item = mapVariableNames.Find(i);
				if(item)
					strObj.str = *item;
				mapNameTable.Push(strObj);
			}

			unsigned int strPointersSize = 4*mapNameTable.Size();
			memcpy(chunkHeader, "MEXP", 4);
			WriteLittleLong(chunkHeader+4, 4+strPointersSize+GetStringTableSize(mapNameTable, 4+strPointersSize));
			out.Write(chunkHeader, 8);
			WriteStringTable(out, mapNameTable);
		}

		unsigned int newHeaderOffset = static_cast<unsigned int> (out.Size())+8;
		out.Write(header+8, 16);
		WriteLittleLong(header+4, newHeaderOffset);
		out.Seek(4);
		out.Write(header+4, 4);
	}

	return String(out.Data(), out.Size());
}

unsigned int Assembler::GetByteCodeSize() const
{
	unsigned int size = 0;
	ByteCode *pCode = code[CODE_HEAD];
	while(!pCode->IsNull())
	{
		size += pCode->GetSize();
		pCode = pCode->Next();
	}
	return size;
}

unsigned int Assembler::GetStringTableSize(StringTable &table, unsigned int offset)
{
	unsigned int size = 0;
	for(StringTable::Iterator iter(table);iter.Next();)
	{
		iter->offset = offset + size;
		size += iter->str.Length()+1;
	}
	return size;
}

Assembler::Instruction *Assembler::InstructionByMnemonic(const String &name)
{
	Instruction **instr = instructionMap.Find(name);
	if(instr)
		return *instr;
	return NULL; // Can't find instruction
}

void Assembler::Optimize()
{
	// This function handles switching constant pushes to direct mnemonics.

	ByteCode *pCode = code[CODE_HEAD];

	ByteCode *firstConstPush = NULL;
	ByteCode *lastConstPush = NULL;
	unsigned short constPushes = 0;
	while(!pCode->IsNull())
	{
		if(pCode->IsLabeled()) // Only optimize past labels.
			constPushes = 0;

		if(pCode->OpCode()->name.Compare("PUSHNUMBER") == 0)
		{
			if(constPushes == 0)
				firstConstPush = pCode;
			lastConstPush = pCode;
			constPushes++;
		}
		else
		{
			if(!pCode->OpCode()->direct && pCode->OpCode()->alternate != NULL &&
				(pCode->OpCode()->alternate->args - pCode->OpCode()->args == constPushes))
			{
				int value = firstConstPush->GetArgument(0).Resolve();
				unsigned int nextArgIndex = firstConstPush->SetToAlternate(pCode);
				firstConstPush->SetArgument(nextArgIndex++, value);

				ByteCode *relinkPoint = pCode->Next();
				// Copy and clean up if needed
				if(firstConstPush != lastConstPush)
				{
					ByteCode *start = firstConstPush->Next();
					do
					{
						firstConstPush->SetArgument(nextArgIndex++, start->GetArgument(0));
						start = start->Next();
					}
					while(start != lastConstPush);

					pCode->Unlink();
					delete firstConstPush->Next();
				}
				firstConstPush->Relink(relinkPoint);
				pCode = firstConstPush;
			}

			constPushes = 0;
		}

		pCode = pCode->Next();
	}
}

void Assembler::Parse(Scanner &sc)
{
	while(sc.GetNextToken())
	{
		if(sc->token == TK_Identifier)
		{
			// Check for some predefined keywords
			if(sc->str.Compare("mnemonic") == 0)
			{
				ParseMnemonic(sc, false);
				continue;
			}
			else if(sc->str.Compare("constmnemonic") == 0)
			{
				ParseMnemonic(sc, true);
				continue;
			}
			else if(sc->str.Compare("section") == 0)
			{
				sc.MustGetToken(TK_Identifier);
				if(sc->str.Compare("functions") == 0)
					ParseFunctions(sc);
				else if(sc->str.Compare("load") == 0)
					ParseLoad(sc);
				else if(sc->str.Compare("maparrays") == 0)
					ParseMapArrays(sc);
				else if(sc->str.Compare("mapvariables") == 0)
					ParseMapVariables(sc);
				else if(sc->str.Compare("scripts") == 0)
					ParseScriptsDirectory(sc);
				else if(sc->str.Compare("strings") == 0)
					ParseStringTable(sc);
				else
					sc.ScriptMessage(Scanner::ERROR, "Unknown section \"%s\".", sc->str.Chars());
				continue;
			}

			// If we get a colon we have a label
			String identifier = sc->str;
			if(sc.CheckToken(':'))
			{
				labelIndex.Insert(identifier, Constant(code[CODE_TAIL]->Next()));
				code[CODE_TAIL]->Next()->SetLabeled(true);
				continue;
			}

			Instruction *instr = InstructionByMnemonic(identifier.ToUpper());
			if(instr == NULL)
				sc.ScriptMessage(Scanner::ERROR, "Unknown mnemonic '%s'.", identifier.Chars());

			// Create our code storage.
			code[CODE_TAIL] = code[CODE_TAIL]->Next();
			code[CODE_TAIL]->Allocate(instr);
			unsigned char i = 0;
			int lastValue = 0;
			for(const char* iter = instr->argFormat;*iter != '\0';++iter)
			{
				if(*iter == '-')
				{
					code[CODE_TAIL]->AllocateArgumentSpace(i - 1);
					while(lastValue-- > 0)
					{
						sc.MustGetToken(',');
						if(sc.CheckToken(TK_Identifier))
							code[CODE_TAIL]->SetArgument(i++, sc->str);
						else
						{
							sc.MustGetToken(TK_IntConst);
							code[CODE_TAIL]->SetArgument(i++, sc->number);
						}
					}
					continue;
				}
				else if(*iter == '+')
					continue;

				if(i != 0)
					sc.MustGetToken(',');

				if(sc.CheckToken(TK_Identifier))
				{
					code[CODE_TAIL]->SetArgument(i++, sc->str);
					lastValue = 0;
				}
				else
				{
					sc.MustGetToken(TK_IntConst);
					lastValue = sc->number;
					code[CODE_TAIL]->SetArgument(i++, lastValue);
				}
			}
		}
		else if(sc->token == '}')
			break;
		else
			sc.ScriptMessage(Scanner::ERROR, "Expected assembler instruction.");
	}
}

Assembler::Constant Assembler::ParseConstant(Scanner &sc) const
{
	if(sc.CheckToken(TK_Identifier))
		return Constant(sc->str);
	sc.MustGetToken(TK_IntConst);
	return Constant(sc->number);
}

void Assembler::ParseFunctions(Scanner &sc)
{
	// Not supported by Hexen compatible scripts.
	if(g_format == FORMAT_HEXEN)
		Print(ML_WARNING, "Functions are not supported by Hexen compatible scripts.");
	do
	{
		Function func;
		if(sc.CheckToken(TK_Identifier))
		{
			func.label = sc->str;
			labelIndex.Insert(sc->str, functions.Size());
			sc.MustGetToken(':');
		}

		sc.MustGetToken(TK_IntConst);
		func.numArgs = sc->number;
		sc.MustGetToken(TK_IntConst);
		func.numVars = sc->number;
		sc.MustGetToken(TK_IntConst);
		func.hasReturn = sc->number != 0;
		sc.MustGetToken(TK_Identifier);
		func.address = sc->str;
		functions.Push(func);
	}
	while(sc.CheckToken(','));
}

void Assembler::ParseLoad(Scanner &sc)
{
	// Not supported by Hexen compatible scripts.
	if(g_format == FORMAT_HEXEN)
		Print(ML_WARNING, "Library imports not supported by Hexen compatible scripts.");
	do
	{
		sc.MustGetToken(TK_StringConst);
		loadImports.Push(sc->str);
	}
	while(sc.CheckToken(','));
}

void Assembler::ParseMapArrays(Scanner &sc)
{
	// Not supported by Hexen compatible scripts.
	if(g_format == FORMAT_HEXEN)
		Print(ML_WARNING, "Map arrays are not supported by Hexen compatible scripts.");
	do
	{
		MapArray array;
		array.import = false;
		array.strings = false;
		String label;
		if(sc.CheckToken(TK_Identifier))
		{
			label = sc->str;
			sc.MustGetToken(':');
		}
		sc.MustGetToken(TK_IntConst);
		if(!label.IsEmpty())
		{
			labelIndex.Insert(label, sc->number);
			mapVariableNames.Insert(sc->number, label);
		}
		array.number = sc->number;
		sc.MustGetToken(TK_IntConst);
		array.size = sc->number;
		if(sc.CheckToken(TK_Identifier))
		{
			array.import = true;
			array.label = sc->str;
		}
		else if(sc.CheckToken('{'))
		{
			do
			{
				array.values.Push(ParseConstant(sc));
			}
			while(sc.CheckToken(','));
			sc.MustGetToken('}');
			if(sc.CheckToken(':'))
			{
				sc.MustGetToken(TK_Identifier);
				if(sc->str.Compare("strings") == 0)
					array.strings = true;
				else
					sc.ScriptMessage(Scanner::ERROR, "Expected keyword \"strings\".");
			}
			if(array.values.Size() != array.size)
				sc.ScriptMessage(Scanner::ERROR, "Array defined with %d values, but initialized with %d.", array.size, array.values.Size());
		}
		mapArrays.Push(array);
	}
	while(sc.CheckToken(','));
}

void Assembler::ParseMapVariables(Scanner &sc)
{
	// Not supported by Hexen compatible scripts.
	if(g_format == FORMAT_HEXEN)
		Print(ML_WARNING, "Map variable initialization is not supported by Hexen compatible scripts.");
	do
	{
		MapVariable var;
		var.import = false;
		var.isString = false;
		String label;
		if(sc.CheckToken(TK_Identifier))
		{
			label = sc->str;
			sc.MustGetToken(':');
		}
		sc.MustGetToken(TK_IntConst);
		if(!label.IsEmpty())
		{
			labelIndex.Insert(label, sc->number);
			mapVariableNames.Insert(sc->number, label);
		}
		var.number = sc->number;
		if(sc.CheckToken(TK_Identifier))
		{
			var.import = true;
			var.label = sc->str;
		}
		else if(sc.CheckToken('{'))
		{
			var.value = ParseConstant(sc);
			sc.MustGetToken('}');
			if(sc.CheckToken(':'))
			{
				sc.MustGetToken(TK_Identifier);
				if(sc->str.Compare("strings") == 0)
					var.isString = true;
				else
					sc.ScriptMessage(Scanner::ERROR, "Expected keyword \"strings\".");
			}
		}
		mapVariables.Push(var);
	}
	while(sc.CheckToken(','));
}

void Assembler::ParseMnemonic(Scanner &sc, bool direct)
{
	sc.MustGetToken(TK_IntConst);
	Instruction *instruction = new Instruction();
	instruction->opcode = sc->number;
	instruction->direct = direct;
	instruction->alternate = NULL;
	instruction->args = 0;
	instruction->argsVariable = false;

	sc.MustGetToken(TK_Identifier);
	instruction->name = String(sc->str).ToUpper();

	if(g_verbose)
		Print(ML_NOTICE, "Adding assembly mnemonic %d => %s.", instruction->opcode, instruction->name.Chars());

	// If this is a direct opcode we need to link it with it's indirect variant.
	if(direct)
	{
		sc.MustGetToken(',');
		sc.MustGetToken(TK_Identifier);

		String parentInstr(sc->str.ToUpper());
		Instruction *parent = InstructionByMnemonic(parentInstr);
		if(parent == NULL)
			sc.ScriptMessage(Scanner::ERROR, "Could not find mnemonic '%s', did you define it first?", parentInstr.Chars());
		if(parent->alternate != NULL)
			sc.ScriptMessage(Scanner::WARNING, "Mnemonic '%s' already had an alternate defined.", instruction->name.Chars());
		parent->alternate = instruction;
		instruction->alternate = parent;
	}

	sc.MustGetToken(',');
	// Scan the args format String and collect some information.
	sc.MustGetToken(TK_StringConst);
	instruction->argFormat = sc->str;
	bool first = true;
	for(const char* iter = sc->str;*iter != '\0';++iter)
	{
		switch(*iter)
		{
			case '-':
				if(first)
					sc.ScriptMessage(Scanner::ERROR, "Variable parameter can not be first.");
				else if(instruction->argsVariable)
					sc.ScriptMessage(Scanner::ERROR, "Only one variable parameter is supported.");
				instruction->argsVariable = true;
			case '+':
				break;
			case 'I':
			case 'S':
			case 'B':
				instruction->args++;
				break;
			default:
				sc.ScriptMessage(Scanner::ERROR, "Mnemonic parameters must be either 'B', 'S', 'I', or '-'.");
				break;
		}
		first = false;
	}

	instructionMap.Insert(instruction->name, instruction);
}

void Assembler::ParseScriptsDirectory(Scanner &sc)
{
	bool warnHexenFlags = false;
	bool warnHexenVarCount = false;
	bool warnHexenNamedScripts = false;
	do
	{
		Script script;
		if(sc.CheckToken(TK_StringConst))
		{
			if(!warnHexenNamedScripts && g_format == FORMAT_HEXEN)
			{
				Print(ML_WARNING, "Named scripts are not supported by Hexen compatible scripts. Scripts will be ignored.");
				warnHexenNamedScripts = true;
			}
			script.number = -(++namedScriptIndex);
			script.name = sc->str;
		}
		else
		{
			sc.MustGetToken(TK_IntConst);
			script.number = sc->number;
			if(script.number > 32767 || (g_format == FORMAT_HEXEN && script.number > 999))
				sc.ScriptMessage(Scanner::ERROR, "Script %d out of range.", script.number);
		}
		sc.MustGetToken(TK_IntConst);
		script.type = sc->number;
		sc.MustGetToken(TK_IntConst);
		script.args = sc->number;
		sc.MustGetToken(TK_Identifier);
		script.address = sc->str;

		if(sc.CheckToken('['))
		{
			sc.MustGetToken(TK_IntConst);
			script.varCount = sc->number;
			sc.MustGetToken(']');
			if(!warnHexenVarCount && g_format == FORMAT_HEXEN)
			{
				Print(ML_WARNING, "Script var count changes are not supported by Hexen compatible scripts.");
				warnHexenVarCount = true;
			}
		}
		else
			script.varCount = 20;

		script.flags = 0;
		while(sc.CheckToken(TK_IntConst))
		{
			if(!warnHexenFlags && g_format == FORMAT_HEXEN)
			{
				Print(ML_WARNING, "Script flags are not supported by Hexen compatible scripts.");
				warnHexenFlags = true;
			}
			script.flags |= 1<<(sc->number-1);
		}

		if(script.number < 0 && g_format == FORMAT_HEXEN)
			continue;
		scripts.Push(script);
	}
	while(sc.CheckToken(','));
}

void Assembler::ParseStringTable(Scanner &sc)
{
	do
	{
		String label;
		unsigned int expectedPosition = stringTable.Size();
		if(sc.CheckToken(TK_Identifier))
		{
			label = sc->str;
			sc.MustGetToken(':');
		}

		// In order to produce an optimal String table we need to check if the
		// String as already existed.  Unfortunately I can't think of a better
		// way than iterating through all the strings.
		sc.MustGetToken(TK_StringConst);
		unsigned int position = 0;
		for(StringTable::ConstIterator iter(stringTable);iter.Next();)
		{
			if(iter->str.Compare(sc->str) == 0)
			{
				if(!label.IsEmpty())
				{
					labelIndex.Insert(label, position);
					position = -1;
				}
				break;
			}
			position++;
		}
		if(position != -1)
		{
			StringTableObject obj;
			obj.str = sc->str;
			stringTable.Push(obj);
			labelIndex.Insert(label, expectedPosition);
		}
	}
	while(sc.CheckToken(','));
}

const Assembler::Constant *Assembler::ResolveLabel(const String &label) const
{
	return labelIndex.Find(label);
}

void Assembler::WriteStringTable(Stream &out, StringTable &table, bool wasteful, bool encrypt)
{
	char header[12] = {0,0,0,0, WriteLittleLongDirect(table.Size()), 0,0,0,0};
	if(wasteful)
		out.Write(header, 12);
	else
		out.Write(header+4, 4);

	char* data = new char[table.Size()*4];
	unsigned int pos = 0;
	for(StringTable::ConstIterator iter(table);iter.Next();)
	{
		WriteLittleLong(data+pos, iter->offset);
		pos += 4;
	}
	out.Write(data, table.Size()*4);
	delete[] data;
	for(StringTable::Iterator iter(table);iter.Next();)
	{
		if(encrypt)
			out.Write(iter->str.EncryptedChars(iter->offset), iter->str.Length()+1);
		else
			out.Write(iter->str.Chars(), iter->str.Length()+1);
	}
}
