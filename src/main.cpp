/*
** Copyright (c) 2010, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <fstream>
#include <cstdarg>
#include <cstdlib>
#include <cstring>

#include "assembler.h"
#include "disassembler.h"
#include "compiler.h"
#include "main.h"
#include "preprocessor.h"
#include "lib/linkedlist.h"
#include "lib/stream.h"
#include "lib/string.h"

using namespace std;
using namespace ACCPP;

LinkedList<String> includeDirs;

bool g_encryptStrings = false;
ByteCodeFormat g_format = FORMAT_ZDOOM;
Language g_language = LANG_ACSPP;
bool g_verbose = false;

void Print(MessageLevel level, const char* error, ...)
{
	static const char* const levelNames[] = {"Notice", "Warning", "Error"};

	char* errorMsg = new char[strlen(error) + 10];
	sprintf(errorMsg, "%s: %s\n", levelNames[level], error);
	va_list list;
	va_start(list, error);
	vfprintf(stderr, errorMsg, list);
	va_end(list);
	delete[] errorMsg;
	if(level == ML_ERROR)
		exit(0);
}

const char* ReadFileWithSize(const char* filename, unsigned int &size)
{
	// If the file is "-" read from stdin
	if(strcmp(filename, "-") == 0)
	{
		Stream tmp;
		char buffer[1024];
		while(true)
		{
			cin.get(buffer, 1024, '\0');
			if(buffer[0] == '\0')
				break;
			tmp.Write(buffer, strlen(buffer));
		}
		size = tmp.Size();
		char* inBuffer = new char[size+1];
		memcpy(inBuffer, tmp.Data(), size);
		inBuffer[size] = 0;
		return inBuffer;
	}

	ifstream in;
	for(LinkedList<String>::ConstIterator iter = includeDirs.HeadConst();iter.Next();)
	{
		in.open((*iter + "/" + filename).Chars(), ios_base::binary|ios_base::in);
		if(in.is_open())
			break;
		else
			in.clear();
	}
	if(!in.is_open())
	{
		Print(ML_ERROR, "Could not open \"%s\".", filename);
		return NULL;
	}
	in.seekg(0, ios_base::end);
	size = in.tellg();
	char* inBuffer = new char[size+1];
	inBuffer[size] = 0;
	in.seekg(0, ios_base::beg);
	in.read(inBuffer, size);
	if(in.fail() && !in.eof())
	{
		delete[] inBuffer;
		Print(ML_ERROR, "Could not read from \"%s\"", filename);
		return NULL;
	}
	in.close();

	return inBuffer;
}

const char* ReadFile(const char* filename)
{
	unsigned int size;
	return ReadFileWithSize(filename, size);
}

void WriteFile(const char* filename, const char* data, int size)
{
	// "-" is stdout
	if(strcmp(filename, "-") == 0)
	{
		cout << data;
		return;
	}

	ofstream out(filename, ios_base::binary|ios_base::out);
	if(!out.is_open())
	{
		Print(ML_ERROR, "Could not open \"%s\".", filename);
		return;
	}
	out.write(data, size == -1 ? strlen(data) : size);
	if(out.fail())
		Print(ML_ERROR, "Could not write to \"%s\".", filename);
	out.close();
}

int main(int argc, char* argv[])
{
	if(argc <= 1)
	{
		Print(ML_NOTICE, "ACS++ Compiler: no input files.");
		return 0;
	}

	bool preprocess = true;
	bool compile = true;
	bool assemble = true;

	includeDirs.Push(".");

	Preprocessor preprocessor;

	const char* inFile = NULL;
	const char* outFile = NULL;
	const char* disassembleMnemonics = NULL;
	char* deleteFile = NULL; // In case we generate a name for outFile, we can delete[] it later.
	for(int i = 1;i < argc;i++)
	{
		// Just to keep things tidy
		#define CHECKNEXTARG if(++i >= argc) break
		if(argv[i][0] == '-' && argv[i][1] != '-')
		{
			switch(argv[i][1])
			{
				default:
					Print(ML_WARNING, "Unknown parameter \"-%c\"!", argv[i][1]);
					break;
				case 'i':
					CHECKNEXTARG;
				case '\0': // "-" probably means use stdin for input.
					inFile = argv[i];
					break;
				case 'o':
					CHECKNEXTARG;
					outFile = argv[i];
					break;
				case 'D':
				{
					String extDefine(argv[i] + 2);
					int equals = extDefine.IndexOf('=');
					if(equals == -1)
						preprocessor.AddDefine(DefineConstant(extDefine, "", 0, NULL));
					else
						preprocessor.AddDefine(DefineConstant(extDefine.Substr(0, equals), extDefine.Substr(equals+1), 0, NULL));
					break;
				}
				case 'c':
					// Is there a compile only option is GCC? I couldn't find
					// one outside of using the file extension '.i'
					preprocess = false;
					break;
				case 'E':
					compile = false;
					break;
				case 'S':
					assemble = false;
					break;
				case 'h':
					g_format = FORMAT_HEXEN;
					break;
				case 'I':
					if(argv[i][2] != '\0')
						includeDirs.Push(argv[i] + 2);
					break;
				case 'v':
					g_verbose = true;
					break;
				case 'x':
					CHECKNEXTARG;
					if(strcmp(argv[i], "acs") == 0)
						g_language = LANG_ACS;
					else if(strcmp(argv[i], "acs++") == 0)
						g_language = LANG_ACSPP;
					else if(strcmp(argv[i], "asm") == 0)
					{
						g_language = LANG_ASM;
						compile = false;
					}
					else
						Print(ML_ERROR, "Unknown language \"%s\".", argv[i]);
			}
		}
		else if(argv[i][0] == '-' && argv[i][1] == '-')
		{
			if(strcmp(argv[i], "--verbose") == 0)
				g_verbose = true;
			else if(strcmp(argv[i], "--encryptstrings") == 0)
				g_encryptStrings = true;
			else if(strcmp(argv[i], "--disassemble") == 0)
			{
				CHECKNEXTARG;
				g_language = LANG_ASM;
				compile = false;
				preprocess = false;
				assemble = false;
				disassembleMnemonics = argv[i];
			}
			else if(strcmp(argv[i], "--format") == 0)
			{
				CHECKNEXTARG;
				if(strcmp(argv[i], "hexen") == 0)
					g_format = FORMAT_HEXEN;
				else if(strcmp(argv[i], "zdoomold") == 0)
					g_format = FORMAT_ZDOOMOLD;
				else if(strcmp(argv[i], "zdoom") == 0)
					g_format = FORMAT_ZDOOM;
				else
					Print(ML_ERROR, "Unkown byte code format \"%s\".", argv[i]);
			}
			else if(strcmp(argv[i], "--help") == 0)
			{
				cout << "Usage: acc++ [options] file...\n"
					"Options:\n"
					"  -c                   Compile only, do not preprocess.\n"
					"  -D<const>=<value>    Add preprocessor constant.\n"
					"  -E                   Preprocess only, do not compile.\n"
					"  -S                   Do not assemble.\n"
					"  --format <fmt>       Switches between bytecode formats:\n"
					"        hexen    - \"ACS\\0\"\n"
					"        zdoomold - \"ACSE\"\n"
					"        zdoom    - \"ACSe\"\n"
					"  -h                   Enable hexen compatibility.\n"
					"  -I<dir>              Add include path.\n"
					"  -i <file>            Use the specified file for input (- for stdin)\n"
					"  -o <file>            Use the specified file for output (- for stdout)\n"
					"  -v or --verbose      Enable verbose error messages.\n"
					"  -x <lang>            Change the language, valid values are:\n"
					"        acs\n"
					"        acs++\n"
					"        asm\n"
					"  --encryptstrings     Encrypts the strings in the string table.\n"
					"  --disassemble <file> Specifies the disassembly mnemonics definition.\n"
					"\n"
					"Writen by Braden \"Blzut3\" Obrzut\n"
					"http://bitowl.com\n";
				return 0;
			}
		}
		else
			inFile = argv[i];
	}

	if(outFile == NULL)
	{
		String inFilename = inFile;
		int ext = inFilename.LastIndexOf('.');
		if(ext == -1)
		{
			deleteFile = new char[inFilename.Length()+3];
			strcpy(deleteFile, inFile);
		}
		else
		{
			inFilename = inFilename.Substr(0, ext);
			deleteFile = new char[inFilename.Length()+3];
			strcpy(deleteFile, inFilename.Chars());
		}
		strcat(deleteFile, ".o");
		outFile = deleteFile;
	}

	String outBuffer;
	if(disassembleMnemonics)
	{
		Disassembler disassembler(disassembleMnemonics);

		unsigned int dataSize;
		const char* tmp = ReadFileWithSize(inFile, dataSize);
		outBuffer = disassembler.Disassemble(tmp, dataSize);
		delete[] tmp;
	}
	else
	{
		// If compilation is disabled, we basically just asked for ASM
		if(!compile && assemble)
			g_language = LANG_ASM;

		preprocessor.AddDefine(DefineConstant("ACCPP", "1", 0, NULL));
		if(g_language != LANG_ASM)
			preprocessor.AddDefine(DefineConstant(g_language == LANG_ACSPP ? "LANG_ACSPP" : "LANG_ACS", "1", 0, NULL));
		else
			preprocessor.AddDefine(DefineConstant("LANG_ASM", "1", 0, NULL));
		if(preprocess)
			outBuffer = preprocessor.ProcessCode(inFile);
		else
		{
			const char* tmp = ReadFile(inFile);
			outBuffer = tmp;
			delete[] tmp;
		}

		if(g_verbose)
		{
			if(!preprocessor.GetLibraryName().IsEmpty())
				Print(ML_NOTICE, "Exporting as library \"%s\".", preprocessor.GetLibraryName().Chars());
			if(preprocessor.GetNumImports() > 0)
			{
				String* imports = new String[preprocessor.GetNumImports()];
				preprocessor.GetImports(imports);
				for(unsigned int i = 0;i < preprocessor.GetNumImports();i++)
					Print(ML_NOTICE, "Importing library \"%s\".", imports[i].Chars());
				delete[] imports;
			}
		}

		if(compile)
		{
			Compiler compiler;
			outBuffer = compiler.Compile(outBuffer);
		}

		if(assemble)
		{
			Assembler assembler;
			outBuffer = assembler.Assemble(outBuffer);
		}
	}
	WriteFile(outFile, outBuffer, outBuffer.Length());

	if(deleteFile != NULL)
		delete[] deleteFile;
	return 0;
}
