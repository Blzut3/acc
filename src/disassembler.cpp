/*
** Copyright (c) 2010, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "disassembler.h"
#include "main.h"
#include "preprocessor.h"
#include "scanner.h"
#include "lib/linkedlist.h"
#include "lib/stream.h"
#include "lib/string.h"

using namespace ACCPP;

// --- So this is the only file we need binary reading functions. --------------

#define ChunkID(a,b,c,d) ((unsigned int)(a|((unsigned int)b<<8)|((unsigned int)c<<16)|((unsigned int)d<<24)))
static inline unsigned int ReadLittleLong(const char* buffer)
{
	return (buffer[0]&0xFF)|((unsigned int)buffer[1]<<8)|((unsigned int)buffer[2]<<16)|((unsigned int)buffer[3]<<24);
}
static inline unsigned short ReadLittleShort(char* buffer)
{
	return buffer[0]|((unsigned short)buffer[1]<<8);
}

// --- Structures --------------------------------------------------------------

struct ScriptPointer
{
	public:
		ScriptPointer() : number(0), type(0), address(0), argCount(0), flags(0), varCount(20)
		{
		}

		unsigned short	number;
		unsigned int	type;
		unsigned int	address;
		unsigned int	argCount;
		unsigned short	flags;
		unsigned short	varCount;

		void	WritePointer(Stream &out)
		{
			out << number << " " << type << " " << argCount << " " << "Script" << number;
			if(varCount != 20)
				out << "[" << varCount << "]";
			if(flags != 0)
				out << " " << flags;
		}
};

// Stores disassembled ranges.
struct Range
{
	public:
		const char*	start;
		const char*	end;
};

// -----------------------------------------------------------------------------

Disassembler::Disassembler(const char* mnemonicsFile)
{
	// We need to know what our instruction set is, so lets read a mnemonics
	// file.
	Preprocessor preprocessor;
	preprocessor.AddDefine(DefineConstant("ACCPP", "1", 0, NULL));
	preprocessor.AddDefine(DefineConstant("LANG_ASM", "1", 0, NULL));
	String mnemonicsData = preprocessor.ProcessCode(mnemonicsFile);

	Scanner sc(mnemonicsData, mnemonicsData.Length());
	while(sc.CheckToken(TK_Identifier))
	{
		if(sc->str.Compare("mnemonic") == 0 || sc->str.Compare("constmnemonic") == 0)
		{
			bool direct = sc->str.Compare("constmnemonic") == 0;
			Instruction instr;
			unsigned int num;

			sc.MustGetToken(TK_IntConst);
			num = sc->number;
			sc.MustGetToken(TK_Identifier);
			instr.mnemonic = sc->str;
			sc.MustGetToken(',');
			if(direct)
			{
				sc.MustGetToken(TK_Identifier);
				sc.MustGetToken(',');
			}
			sc.MustGetToken(TK_StringConst);
			instr.params = sc->str;

			mnemonicMap.Insert(num, instr);
		}
		else
			sc.ScriptMessage(Scanner::ERROR, "Expected mnemonic definition.");
	}
}

String Disassembler::Disassemble(const char* data, unsigned int size)
{
	if(size < 12 && ReadLittleLong(data+4) < size)
		return String();

	this->data = data;
	this->size = size;

	Stream out;
	out << "// Disassembled by ACC++\n#include \"mnemonics.inc\"\n\n";

	// Detect format.
	ByteCodeFormat format = FORMAT_HEXEN;
	const char* chunks;
	if(!ParseHeader(format, chunks))
		return String();

	// Dissassemble the chunks
	LinkedList<ScriptPointer> scripts;
	if(format == FORMAT_HEXEN)
	{
		unsigned int numScripts = ReadLittleLong(chunks);
		const char* scriptPtr = chunks + 4;
		const char* stringPtr = scriptPtr + numScripts*12;
		if(numScripts > 0)
		{
			out << "section scripts\n";
			while(numScripts-- > 0)
			{
				ScriptPointer ptr;
				ptr.number = ReadLittleLong(scriptPtr)%1000;
				ptr.type = ReadLittleLong(scriptPtr)/1000;
				ptr.address = ReadLittleLong(scriptPtr+4);
				ptr.argCount = ReadLittleLong(scriptPtr+8);

				scripts.Push(ptr);
				scriptPtr += 12;

				out << "\t";
				ptr.WritePointer(out);
				if(numScripts != 0)
					out << ",";
				out << "\n";
			}
		}

		unsigned int numStrings = ReadLittleLong(stringPtr);
		if(numStrings > 0)
		{
			out << "section strings\n";

			stringPtr += 4;
			while(numStrings-- > 0)
			{
				out << "\t\"" << Scanner::Escape((data + ReadLittleLong(stringPtr))) << "\"";
				if(numStrings != 0)
					out << ",";
				out << "\n";
				stringPtr += 4;
			}
		}
	}

	// Disassemble the scripts!
	LinkedList<Range> disassembledRanges;
	for(LinkedList<ScriptPointer>::Iterator iter(scripts);iter.Next();)
	{
		// Check the ranges first!
		bool disassembled = false;
		for(LinkedList<Range>::ConstIterator range(disassembledRanges);range.Next();)
		{
			if(range->start <= data+iter->address && range->end >= data+iter->address)
			{
				disassembled = true;
				break;
			}
		}
		if(disassembled)
			continue;

		Range range;
		out << "Script" << iter->number << ":\n";
		const char* disasm = data + iter->address;
		range.start = disasm;
		unsigned int opcode = 0;
		Instruction instr;
		do
		{
			opcode = ReadLittleLong(disasm);
			disasm += 4;
			if(FindInstruction(opcode, instr))
			{
				out << "\t" << instr.mnemonic;
				char lastArgType = 'I';
				unsigned int lastArg = 0;
				for(const char* arg = instr.params.Chars();arg;++arg)
				{
					if(*arg == '+')
						continue;

					char thisArgType = *arg == '-' ? lastArgType : *arg;
					unsigned int thisArg = 0;
					while(*arg != '-' || lastArg-- > 0) // First condition will break later
					{
						thisArg = ReadLittleLong(disasm);
						disasm += 4;
						if(arg != instr.params.Chars())
							out << ",";
						out << " " << thisArg;
						if(*arg != '-')
							break;
					}
					lastArg = thisArg;
				}
				out << "\n";
			}
		}
		while(opcode != 1 && disasm < chunks); // Terminate should be 1 and ZDoom doesn't allow code past the footer.

		range.end = disasm;
		disassembledRanges.Push(range);
	}

	out.Write("", 1);
	return out.Data();
}

bool Disassembler::FindInstruction(unsigned int code, Instruction &instr) const
{
	const Instruction *iter = mnemonicMap.Find(code);
	if(iter != NULL)
	{
		instr = *iter;
		return true;
	}
	return false;
}

bool Disassembler::ParseHeader(ByteCodeFormat &fmt, const char* &chunks) const
{
	chunks = data + ReadLittleLong(data+4);
	switch(ReadLittleLong(data))
	{
		case ChunkID('A','C','S',0):
			switch(ReadLittleLong(chunks-4))
			{
				case ChunkID('A','C','S','E'):
					fmt = FORMAT_ZDOOMOLD;
					chunks = data + ReadLittleLong(data-8);
					break;
				case ChunkID('A','C','S','e'):
					fmt = FORMAT_ZDOOM;
					chunks = data + ReadLittleLong(data-8);
					break;
				default:
					fmt = FORMAT_HEXEN;
					break;
			}
			break;
		case ChunkID('A','C','S','E'):
			fmt = FORMAT_ZDOOMOLD;
			break;
		case ChunkID('A','C','S','e'):
			fmt = FORMAT_ZDOOM;
			break;
		default:
			return false;
	}
	if(g_verbose)
		Print(ML_NOTICE, "Compiled ACS script detected as: %s\n", fmt == FORMAT_HEXEN ? "Hexen" : (fmt == FORMAT_ZDOOMOLD ? "ZDoom Old" : "ZDoom New"));
	return true;
}
