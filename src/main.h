/*
** Copyright (c) 2010, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __MAIN_H__
#define __MAIN_H__

enum ByteCodeFormat
{
	FORMAT_ZDOOM,
	FORMAT_ZDOOMOLD,
	FORMAT_HEXEN
};

enum Language
{
	LANG_ACSPP,
	LANG_ACS,
	LANG_ASM,
	LANG_PREPROCESSOR
};

enum MessageLevel
{
	ML_NOTICE,
	ML_WARNING,
	ML_ERROR
};

void		Print(MessageLevel level, const char* error, ...);
const char*	ReadFile(const char* filename);
void		WriteFile(const char* filename, const char* data, int size=-1);

extern bool g_encryptStrings;
extern ByteCodeFormat g_format;
extern Language g_language;
extern bool	g_verbose;

#define WriteLittleLongDirect(integer) (char)(integer&0xFF),(char)((integer>>8)&0xFF),(char)((integer>>16)&0xFF),(char)((integer>>24)&0xFF)
#define WriteLittleShortDirect(integer) (char)(integer&0xFF),(char)((integer>>8)&0xFF)
static inline void WriteLittleLong(char* buffer, unsigned int value)
{
	buffer[0] = (char)(value&0xFF);
	buffer[1] = (char)((value>>8)&0xFF);
	buffer[2] = (char)((value>>16)&0xFF);
	buffer[3] = (char)((value>>24)&0xFF);
}
static inline void WriteLittleShort(char* buffer, unsigned short value)
{
	buffer[0] = (char)(value&0xFF);
	buffer[1] = (char)((value>>8)&0xFF);
}

#endif /* __MAIN_H__ */
