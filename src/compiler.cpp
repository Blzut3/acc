/*
** Copyright (c) 2010, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "compiler.h"
#include "expression.h"
#include "main.h"
#include "scanner.h"
#include "type.h"
#include "lib/string.h"

#include <cstdio>

using namespace ACCPP;

Compiler::Compiler() : currentScope(0)
{
	types = new TypeHierarchy();
}

Compiler::~Compiler()
{
	delete types;
}

String Compiler::Compile(const String &in)
{
	Scanner sc(in, in.Length());

	return Compile(sc);
}

String Compiler::Compile(Scanner &sc)
{
	// Clear
	out.Clear();

	while(sc.TokensLeft())
	{
		if(sc.CheckToken(TK_Identifier))
		{
			if(sc->str.Compare("asm") == 0)
			{
				out << ParseAssemblyBlock(sc);

				continue;
			}
			else if(sc->str.Compare("class") == 0)
			{
				const Type *parent = NULL;
				sc.MustGetToken(TK_Identifier);
				String newClassName(sc->str);
				if(sc.CheckToken(':'))
				{
					sc.MustGetToken(TK_Identifier);
					parent = types->GetType(sc->str);
					if(!parent || parent->IsPrimitive() || parent->IsForwardDeclared())
						sc.ScriptMessage(Scanner::ERROR, "Attempt to extend an incomplete type.");
				}
				Type *newType = types->CreateType(newClassName, parent);
				sc.MustGetToken('{');
				while(!sc.CheckToken('}'))
				{
					bool asmFunction = false;

					sc.MustGetToken(TK_Identifier);
					if(sc->str.Compare("asm") == 0)
					{
						asmFunction = true;
						sc.MustGetToken(TK_Identifier);
					}

					if(sc->str.Compare("public") == 0 || sc->str.Compare("protected") == 0 || sc->str.Compare("private") == 0)
					{
						sc.MustGetToken(TK_Identifier);
						const Type *retType = types->GetType(sc->str);
						sc.MustGetToken(TK_Identifier);
						String functionName(sc->str);
						sc.MustGetToken('(');
						while(!sc.CheckToken(')'))
						{
						}
						ParseAssemblyBlock(sc);
					}
					else
						sc.ScriptMessage(Scanner::ERROR, "Expected member visibility (public/protected/private).");
				}

				continue;
			}
			else
				sc.Rewind();
		}

		out << ParseScript(sc);
	}
	

	DumpStringTable();

	return String(out.Data(), out.Size());
}

void Compiler::DumpStringTable()
{
	if(stringTable.Size() > 0)
	{
		out << "section strings\n";
		bool firstString = true;
		HashMap<String, String>::ConstIterator iter(stringTable);
		while(iter.Next())
		{
			if(!firstString)
				out << "," << "\n";
			else
				firstString = false;

			out << "\t" << *iter << ": \"" << Scanner::Escape(iter.Key()) << "\"";
		}
		out << "\n";
	}
}

String Compiler::GetString(const String &lookup)
{
	String *item = stringTable.Find(lookup);
	if(item == NULL)
	{
		char label[13];
		sprintf(label, "Str%09X", static_cast<unsigned int>(stringTable.Size()));

		stringTable.Insert(lookup, label);
		return label;
	}
	return *item;
}

Symbol *Compiler::FindSymbol(const String &name)
{
	return symbolTable.Find(name);
}

String Compiler::ParseAssemblyBlock(Scanner &sc)
{
	Stream out;

	sc.MustGetToken('{');
	unsigned int blockStart = sc.GetScanPos();
	unsigned int blockEnd;
	unsigned int levelCounter = 0;
	do
	{
		blockEnd = sc.GetScanPos();
		sc.GetNextToken();
		if(sc->token == '}')
		{
			if(levelCounter == 0)
			{
				out << String(sc.GetData()+blockStart, blockEnd-blockStart) << "\n";
				break;
			}
			levelCounter--;
		}
		else if(sc->token == '{')
			levelCounter++;
	}
	while(sc.TokensLeft());

	return String(out.Data(), out.Size());
}

String Compiler::ParseScript(Scanner &sc)
{
	unsigned int localVar = 0;
	Symbol *localSymbol;

	Stream out;

	sc.MustGetToken('{');
	while(!sc.CheckToken('}'))
	{
		bool assignLocal = false;

		// Variable declaration
		if(sc.CheckToken(TK_Identifier))
		{
			const Type *varType = types->GetType(sc->str);
			if(varType)
			{
				sc.MustGetToken(TK_Identifier);
				symbolTable.Insert(sc->str, Symbol(localVar++, Symbol::LOCAL, sc->str, varType));
				assignLocal = true;
				localSymbol = FindSymbol(sc->str);
				if(!sc.CheckToken('='))
				{
					sc.MustGetToken(';');
					continue;
				}
			}
			else
				sc.Rewind();
		}

		ExpressionNode *node = ExpressionNode::ParseExpression(*this, sc, out);
		node->DumpExpression(out);
		delete node;
		sc.MustGetToken(';');

		if(assignLocal)
			out << localSymbol->AssignSymbol() << "\n";
	}

	return String(out.Data(), out.Size());
}

void Compiler::PopScope()
{
	for(HashMap<String, Symbol>::Iterator item(symbolTable);item.Next();)
	{
		if(item->GetLocal() == currentScope)
			symbolTable.Delete(item);
	}
	--currentScope;
}

void Compiler::PushScope()
{
	++currentScope;
}
