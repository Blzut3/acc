/*
** Copyright (c) 2010, Braden "Blzut3" Obrzut
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * The names of its contributors may be used to endorse or promote
**       products derived from this software without specific prior written
**       permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
** INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
** THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <cstring>

#include "main.h"
#include "preprocessor.h"
#include "scanner.h"
#include "lib/stack.h"
#include "lib/stream.h"
#include "lib/string.h"

using namespace ACCPP;

typedef HashMap<String, const DefineConstant>::ConstIterator DefineIterator;

DefineConstant::DefineConstant(const String &name, const String &replacement, short numVars, const char* const varList[]) : name(name), replacement(replacement), numVars(numVars)
{
	if(numVars != 0)
	{
		// Define is a macro, so scan for identifiers and match it with the variables.
		Scanner sc(replacement, replacement.Length());
		int offset = 0;
		bool quoteOnReplace = false;

		while(sc.GetNextToken())
		{
			if(sc->token != TK_Identifier && quoteOnReplace)
				quoteOnReplace = false;

			if(sc->token == TK_Identifier)
			{
				for(short i = 0;i < numVars;i++)
				{
					if(sc->str.Compare(varList[i]) == 0)
					{
						Replace r;
						r.position = sc.GetPos() + offset - sc->str.Length();
						r.index = i;
						r.length = sc->str.Length();

						if(quoteOnReplace)
						{
							r.length--;

							this->replacement.Replace(sc.GetPos() + offset - sc->str.Length() - 1, 1, "\"");
							this->replacement.Replace(sc.GetPos() + offset - 1, 1, "\"");
							quoteOnReplace = false;
						}

						replaces.Push(r);
						break;
					}
				}
			}
			else if(sc->token == TK_MacroConcat)
			{
				this->replacement.Erase(sc.GetLinePos() + offset, 2);
				offset -= 2;
			}
			else if(sc->token == '#')
			{
				// This should be true if there's no whitespace.
				if(sc.GetPos()-1 == sc.GetLinePos())
					quoteOnReplace = true;
			}
		}

		if(sc.GetPos() != replacement.Length())
			this->replacement.Erase(sc.GetPos() + offset);
	}
}

String DefineConstant::DoReplace(const char* const parameters[]) const
{
	if(numVars != 0 && parameters != NULL)
	{
		String out(replacement);
		int offset = 0;
		for(LinkedList<Replace>::ConstIterator iter = replaces.HeadConst();iter.Next();)
		{
			out.Replace(iter->position + offset, iter->length, parameters[iter->index]);
			offset += strlen(parameters[iter->index]) - iter->length;
		}
		return out;
	}
	return replacement;
}

////////////////////////////////////////////////////////////////////////////////

void Preprocessor::AddDefine(const DefineConstant &constant)
{
	constantMap.Insert(constant.GetName(), constant);
}

void Preprocessor::GetImports(String* buffer) const
{
	unsigned int i = 0;
	for(LinkedList<String>::ConstIterator iter = importList.HeadConst();iter.Next();)
	{
		buffer[i++] = *iter;
	}
}

String Preprocessor::ProcessCode(const char* filename, bool importing, bool dumpImports)
{
	const DefineConstant *item = constantMap.Find("LANG_ASM");
	bool isASM = (item != NULL);

	const char* inBuffer = ReadFile(filename);
	String in = inBuffer;
	delete[] inBuffer;

	// First we want to normalize the line endings.  We'll use UNIX style.
	for(int i = 0;(i = in.IndexOf("\n\r", i)) != -1;)
		in.Replace(i, 2, "\n");
	for(int i = 0;(i = in.IndexOf('\r', i)) != -1;)
		in.Replace(i++, 1, "\n");

	// We'll use a stringstream to make life a little easier.
	Stream out;
	Scanner sc(in.Chars(), in.Length());
	sc.SetScriptIdentifier(filename);
	sc.GetNextToken();

	// Now we need to get each line.
	int pos = 0;
	int ignore = 0;
	int ifLevel = 0;
	int setLine = 0;
	while(sc->token != TK_NoToken)
	{
		if(sc->token == '#')
		{
			if(pos != sc.GetPos()-1 && ignore == 0)
			{
				if(setLine)
				{
					out << "/*meta:" << filename << ":" << setLine << "*/";
					setLine = 0;
				}
				out << in.Substr(pos, sc.GetPos()-pos-1);
			}

			sc.MustGetToken(TK_Identifier);
			if(sc->str.Compare("define") == 0)
			{
				sc.MustGetToken(TK_Identifier);
				String name = sc->str;
				short numVars = 0;
				Stack<String> params;
				if(sc.CheckToken('('))
				{
					do
					{
						sc.MustGetToken(TK_Identifier);
						numVars++;
						params.Push(sc->str);
					}
					while(sc.CheckToken(','));
					sc.MustGetToken(')');
				}

				char** varList = new char*[numVars];
				for(short i = numVars;i-- > 0;)
				{
					String param = params.Pop();
					varList[i] = new char[param.Length()+1];
					strcpy(varList[i], param);
				}
				int newLine = sc.SkipLine();
				if(sc.GetLine() != newLine)
					setLine = newLine;

				AddDefine(DefineConstant(name.Chars(), sc->str, numVars, varList));
				if(g_verbose)
					Print(ML_NOTICE, "Adding Define: %s as \"%s\".", name.Chars(), sc->str.Chars());
				for(short i = 0;i < numVars;i++)
					delete[] varList[i];
				delete[] varList;
			}
			else if(sc->str.Compare("library") == 0)
			{
				sc.MustGetToken(TK_StringConst);
				if(!importing)
					libName = sc->str;
				else
					importList.Push(sc->str);
			}
			else if(sc->str.Compare("include") == 0 || sc->str.Compare("import") == 0)
			{
				bool import = sc->str.Compare("import") == 0;
				sc.MustGetToken(TK_StringConst);
				// We will use import blocks to simply preprocessing work.
				// Note that we don't need to enter an import block if we're already in one.
				if(!isASM || !import)
				{
					if(import && !importing)
						out << "import{";
					out << "/*meta:" << sc->str.Chars() << ":1*/" << ProcessCode(sc->str.Chars(), import, false);
					if(import && !importing)
						out << "}";
					out << "\n";
				}
				else
					ProcessCode(sc->str.Chars(), import, false);
				sc.SkipLine();
				setLine = sc.GetLine();
			}
			else if(sc->str.Compare("ifdef") == 0 || sc->str.Compare("ifndef") == 0)
			{
				bool invert = sc->str.Compare("ifndef") == 0;

				ifLevel++;
				if(ignore > 0)
					ignore++;
				else
				{
					sc.MustGetToken(TK_Identifier);
					const DefineConstant *item = constantMap.Find(sc->str);
					if((!invert && item == NULL) || (invert && item != NULL))
						ignore++;
				}
				sc.SkipLine();
			}
			else if(sc->str.Compare("endif") == 0)
			{
				ifLevel--;
				if(ifLevel < 0)
					sc.ScriptMessage(Scanner::ERROR, "Unmatched #endif.\n");
				if(ignore > 0)
					ignore--;
				sc.SkipLine();
				setLine = sc.GetLine();
			}
			else if(sc->str.Compare("error") == 0)
			{
				sc.MustGetToken(TK_StringConst);
				sc.ScriptMessage(Scanner::ERROR, "%s", sc->str.Chars());
			}
			else if(sc->str.Compare("warning") == 0)
			{
				sc.MustGetToken(TK_StringConst);
				sc.ScriptMessage(Scanner::WARNING, "%s", sc->str.Chars());
				sc.SkipLine();
			}
			if((setLine || sc.GetPos() != sc.GetScanPos()) && ignore == 0)
				setLine = sc.GetLine();
			pos = sc.GetPos();
			sc.GetNextToken();
			continue;
		}
		else
		{
			if(ignore > 0)
			{
				sc.SkipLine();
				sc.GetNextToken();
				continue;
			}

			if(setLine)
			{
				// If we skipped over some whitespace we can adjust the position
				if(setLine != sc.GetLine())
					pos = sc.GetPos()-sc->str.Length()-sc.GetLinePos();
				out << "/*meta:" << filename << ":" << sc.GetLine() << "*/";
				setLine = 0;
			}

			int expectedLine = sc.GetLine();
			do
			{
				if(expectedLine != sc.GetLine())
					break;

				if(sc->token == TK_Identifier)
				{
					const String itemKey(sc->str);
					const DefineConstant *item = constantMap.Find(itemKey);
					if(item != NULL)
					{
						if(item->GetNumParameters() > 0)
						{
							// macro evaluation.
							int macroStart = sc.GetPos();
							sc.MustGetToken('(');
							char** paramList = new char*[item->GetNumParameters()];
							int numParameters = 0;
							int paramStart = sc.GetPos();
							int paramEnd = paramStart;
							int level = 0;
							while(sc.GetNextToken())
							{
								switch(sc->token)
								{
									case '(':
										level++;
										break;
									case ')':
										level--;
										if(level >= 0)
											break;
									case ',':
										paramEnd = sc.GetPos()-1;
										paramList[numParameters] = new char[paramEnd-paramStart+1];
										strcpy(paramList[numParameters], in.Substr(paramStart, paramEnd-paramStart).Chars());
										numParameters++;
										if(numParameters > item->GetNumParameters())
											sc.ScriptMessage(Scanner::ERROR, "Macro %s only takes %d parameters.", itemKey.Chars(), item->GetNumParameters());

										if(level < 0)
										{
											if(numParameters != item->GetNumParameters())
												sc.ScriptMessage(Scanner::ERROR, "Macro %s expected %d parameters, but got %d instead.", itemKey.Chars(), item->GetNumParameters(), numParameters);
											goto FinishParameters;
										}
										paramStart = sc.GetPos();
										break;
								}
							}
						FinishParameters:
							if(numParameters != item->GetNumParameters())
								sc.ScriptMessage(Scanner::ERROR, "Macro %s requires %d parameters, but got %d instead.", itemKey.Chars(), item->GetNumParameters(), numParameters); 
							out << in.Substr(pos, macroStart-pos-itemKey.Length()) << item->DoReplace(paramList);
							for(int i = 0;i < item->GetNumParameters();i++)
								delete[] paramList[i];
							delete[] paramList;
						}
						else
							out << in.Substr(pos, sc.GetPos()-pos-itemKey.Length()) << item->DoReplace(NULL);
						pos = sc.GetPos();
					}
				}
			}
			while(sc.GetNextToken());
		}
	}

	if(pos != sc.GetPos())
		out << in.Substr(pos);

	// Dump some ASM for libraries
	if(dumpImports && importList.Size() > 0)
	{
		if(!isASM)
			out << "asm {\n";

		if(importList.Size() > 0)
		{
			out << "section load\n";
			bool firstItem = true;
			for(LinkedList<String>::ConstIterator iter = importList.HeadConst();iter.Next();)
			{
				if(!firstItem)
					out << ",\n";
				else
					firstItem = false;
				out << "\t\"" << *iter << "\"";
			}
		}

		if(!isASM)
			out << "\n}";
		else
			out << "\n";
	}

	out.Write("", 1);
	return out.Data();
}
