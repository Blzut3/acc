#/bin/sh

# Conversion script written by DavidPH
# Note: Remove comments before processing.

grep '^[ 	]*[0-9]' zspecial.acs |
grep --only-matching '[0-9]*.*' |
awk -F/ '{print $1}' | # For comments.
sort -n |
awk -F: '{print $1 "\n" $2}' |
while read number
do
	read name

	args="$(echo "${name}" | grep --only-matching '(.*)')"

	name="$(expr substr "${name}" 1 \( length "${name}" - length "${args}" - 2 \))"

	arg1="$(expr substr "${args}" 2 1)"
	arg2="$(expr substr "${args}" \( length "${args}" - 1 \) 1)"

	if test "${arg1}" = "${arg2}"
	then
		echo "__SPECIAL${arg1}(${number}, ${name})"
	else
		echo "__SPECIAL${arg1}(${number}, ${name}) // ${arg2}"
	fi
done
